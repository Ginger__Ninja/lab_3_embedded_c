#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "data_logging.h"
#include "pendulum.h"



/* Local Function prototypes */

static void log_pendulum(void *argument);



/* RTOS Init variables*/
// Declare timer id and attr variables
osTimerId_t     _logging_timer_id;  
osTimerAttr_t   _logging_timer_attr = 
{
    .name = "loggingTimer"
};


/* Variable declarations */

uint16_t logCount;
float pot;



/* Functions */

static void log_pendulum(void *argument){
    UNUSED(argument);

    pot = pendulum_read_voltage();

    printf("%f,%.2f\n",logCount*0.005,pot);

    logCount++;


    if (logCount*0.005 == 2) pend_logging_stop();

}


void logging_init(void){
    // Init a periodic timer with the log_pendulum function as the call back, onces period is up
    _logging_timer_id = osTimerNew(log_pendulum, osTimerPeriodic, NULL, &_logging_timer_attr);
    
}


void pend_logging_start(void){
    
    if(!osTimerIsRunning(_logging_timer_id)){
        // Init log count to zero for new logging period
        logCount = 0;

        // Start 200 Hz timer
        osTimerStart(_logging_timer_id, 5U); // period = 1/frequency, 5U is 0.005/sec or 200 Hz

    }

}


void pend_logging_stop(void){

    if(osTimerIsRunning(_logging_timer_id)){
        // Stop timer
        osTimerStop(_logging_timer_id);

    }

}