#include <stdint.h>
#include <stdlib.h>
#include "cmsis_os2.h"
#include "stm32f4xx_hal.h"
#include "uart.h"
#include "servo.h"

static TIM_HandleTypeDef htim4;
static TIM_OC_InitTypeDef sConfigPWM;

int pulse;

void servo_init(void){

   // Enable timer 3 clock
    __HAL_RCC_TIM4_CLK_ENABLE();

    // Enable GPIOA clock
    __HAL_RCC_GPIOB_CLK_ENABLE();

    // Initialise PA6 as required
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_6;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF2_TIM4;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    // Initialise timer 3 as required
    htim4.Instance = TIM4;
    htim4.Init.Prescaler = 73;
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim4.Init.Period = 20000;
    htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;

    // Configure timer 3 channel 1 as required
    sConfigPWM.OCMode = TIM_OCMODE_PWM1;
    sConfigPWM.Pulse = 1000;
    sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;

    //  Initialise timer 3 in PWM mode
    HAL_TIM_PWM_Init(&htim4);

    // Configure timer 3 channel 1 in PWM mode
    HAL_TIM_PWM_ConfigChannel(&htim4,&sConfigPWM,TIM_CHANNEL_1);

    //__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,1500);

    // Start PWM timer
    HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);

    
}

void servo_1_set(uint8_t angle){

    pulse = (int)((((float)angle)/180.0)*(3200-500)+500);

    __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,pulse);


}


