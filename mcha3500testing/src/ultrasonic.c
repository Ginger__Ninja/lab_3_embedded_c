#include <stdint.h>
#include <stdlib.h>
#include "cmsis_os2.h"
#include "stm32f4xx_hal.h"
#include "uart.h"
#include "ultrasonic.h"
#include <math.h>


static TIM_HandleTypeDef htim9;
static TIM_OC_InitTypeDef sConfigUS;

uint8_t pin;
uint32_t start, end;
uint8_t distance_scaled;

void ultrasonic_init(void){

    // Enable GPIOB clock
    __HAL_RCC_GPIOB_CLK_ENABLE();

 
    // Initialise PC11 as uSart RX
    GPIO_InitTypeDef  GPIO_InitStructure;
    
    GPIO_InitStructure.Pin = GPIO_PIN_13;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.Pin = GPIO_PIN_12;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_PULLUP;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);



    HAL_NVIC_SetPriority(EXTI15_10_IRQn, 3, 0);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

    // https://www.titanwolf.org/Network/q/17b530e1-2893-4736-ad5e-ee94734a9ee1/y
    // Timer method from here
    __HAL_RCC_TIM5_CLK_ENABLE();
    TIM5->PSC = HAL_RCC_GetPCLK1Freq()/1000 - 1;
    TIM5->CR1 = TIM_CR1_CEN;

}

// Sends pulse to the ultrasonic sensor, and makes sure the TIM5->CNT Register does not overflow by setting this to 0 after each call
void US_trig(void){

    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, 1); 
    osDelay(1);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, 0); 
    TIM5->CNT = 0;

    osDelay(10);

}

// Returns the distance in cm (max of 250) as type uint8_t
uint8_t US_get_distance(void){

    // Sent trig pulse to the ultrasonic sensor
    US_trig();

    // 100000.0 is period of timer, 180.0 is range in cm when the division is = 1, /1000*100
    distance_scaled = (uint8_t)round((float)((((double)(end-start)/100000.0)*180.0)/10.0));

    if (distance_scaled > 250) distance_scaled = 250;

    return distance_scaled;

}

// prints the distance that is recevied from the US_get_distance functions
void US_test(void){
    
    printf("Distance: %d\n",US_get_distance());

}


// Interupt callback, returns time between start and end of echo pulse
void US_echo(void){
    
    pin = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13);
    
    if (pin == 1){
        start = TIM5->CNT;
    } else {
        end = TIM5->CNT;
    }
        
}


// PB13 Interupt, for ultrasonic echo
void EXTI15_10_IRQHandler(void)
{   
    US_echo();
    
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}