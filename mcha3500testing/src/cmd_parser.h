#ifndef CMD_PARSER_H
#define CMD_PARSER_H

#include "cmd_line_buffer.h"

void cmd_parse(char *);

static void _cmd_getPotentiometerVoltage(int, char *[]);
static void _cmd_startLogging(int, char *[]);

#endif
