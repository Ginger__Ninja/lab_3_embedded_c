#include "uart_BT.h"

#include <stdint.h>
#include <stdlib.h>
#include "cmsis_os2.h"
#include "stm32f4xx_hal.h"
#include "uart.h"
#include "ultrasonic.h"
#include "servo.h"

// Useful links
// https://controllerstech.com/uart-receive-in-stm32/

// Handles
static UART_HandleTypeDef husart3;
static DMA_HandleTypeDef hdma_usart3_rx;


uint16_t USARTInput[10]; // Amount of bytes read by the DMA, should be set between 10 and 30 for best results
//, any less can cause instability and any more can slow it down too much

uint8_t data_ordered[4]; // Amount of desired bytes, at the moment it is: 4 - L and R sliders, buttons, switches
/*
[0] = Left motor speed reference   [0,200] (-100,100)
[1] = Right motor speed reference  [0,200] (-100,100)
[2] = Buttons                      [1,2,4,8 -> 0-16]
[3] = Switches                     [1,2,4,8 -> 0-16]
*/

// App Receives this data every 250 ms, can be made faster but this will slow down the graphics and transmission
uint8_t data_send[5];
/*
[0] = Start bit                    [255]
[1] = Left motor speed measured    [0,200] (-100,100)
[2] = Right motor speed measured   [0,200] (-100,100)
[3] = Ultrasonic distance          [0,200] (cm)
[4] = Angle                        [0,180] (-90,90)
*/

// Threads

static void usart_receive_thread(void *arg);
static void usart_transmit_thread(void *arg);

static osThreadId_t _usartReceiveThreadID;
static osThreadAttr_t _usartReceiveThreadAttr = 
{
    .name = "usartReceive",
};

static osThreadId_t _usartTransmitThreadID;
static osThreadAttr_t _usartTransmitThreadAttr = 
{
    .name = "usartTransmit",
};

void usart_receive_thread_init(void)
{

    // CMSIS-RTOS API v2 Timer Documentation: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/group__CMSIS__RTOS__TimerMgmt.html
    _usartReceiveThreadID = osThreadNew(usart_receive_thread, NULL, &_usartReceiveThreadAttr);   // Create the thread in the OS scheduler. 

}

void usart_transmit_thread_init(void)
{

    // CMSIS-RTOS API v2 Timer Documentation: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/group__CMSIS__RTOS__TimerMgmt.html
    _usartTransmitThreadID = osThreadNew(usart_transmit_thread, NULL, &_usartTransmitThreadAttr);   // Create the thread in the OS scheduler. 

}

void usart_receive_thread(void *arg){
    UNUSED(arg);
    while(1)
    {
        usart_receive();
        // osDelay(1); // Set to delay of 1 when in real world, testing at 100
        osDelay(1);
    }
}

void usart_transmit_thread(void *arg){
    UNUSED(arg);
    while(1)
    {
        usart_transmit();
        osDelay(100);
    }
}


// Hardware


void usart_BT_init(void){

    // Enable GPIOC clock for USART3 RX pin
    __HAL_RCC_GPIOC_CLK_ENABLE();
    
    // Initialise PC11 as uSart RX
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_11 | GPIO_PIN_10;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_PULLUP;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF7_USART3;

    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    // Enable USART3 clock
    __HAL_RCC_USART3_CLK_ENABLE();
    
    husart3.Instance = USART3;
    // Default HC-06 settings (should be fine for what I want to do with it)
    husart3.Init.BaudRate = 9600; // Default baud rate of HC-06
    husart3.Init.WordLength = UART_WORDLENGTH_8B; // Default transmission length
    husart3.Init.StopBits = UART_STOPBITS_1;
    husart3.Init.Parity = UART_PARITY_NONE;
    husart3.Init.Mode = UART_MODE_TX_RX;
    
    HAL_UART_Init(&husart3); 

    // Setup interupts for USARt3, if needed
    HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);

    // DMA1 controller clock enable for direct periph -> memory mode
    __HAL_RCC_DMA1_CLK_ENABLE();

    // DMA Initialise, USART3 is on DMA1 stream:1 channel:4
    hdma_usart3_rx.Instance = DMA1_Stream1;
    hdma_usart3_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_usart3_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart3_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart3_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart3_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD; // 16 bits
    hdma_usart3_rx.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD; // 16 bits
    hdma_usart3_rx.Init.Mode = DMA_CIRCULAR; // Repeats data collection continuously
    hdma_usart3_rx.Init.Priority = DMA_PRIORITY_LOW;
        
    HAL_DMA_Init(&hdma_usart3_rx);

    // Link the DMA to the USART
    __HAL_LINKDMA(&husart3, hdmarx, hdma_usart3_rx);


    // Enable interupt handler to handle DMA transfer completion
    // When this occurs handle data to pull data bytes out
    HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
    
}


void DMA1_Stream1_IRQHandler(void)
{
    // The data from the DMA is longer than the actual data wanted, this is so that there is a higher chance that the
    // data transmission was successful, in this the DAM data recevied is 10 bytes long -> stored in USARTInput
    for (int i = 0; i < sizeof(USARTInput); i++)
    {
        // Pull the data from between two start bits, as this must contain the full amount of data, 4 bytes by default
        if (USARTInput[i] == 255 && USARTInput[i+5] == 255 && USARTInput[i+3] < 16 && USARTInput[i+4] < 16){
            // Was having some weird issues with random values appearing in [2] and [3], so these are checked to make sure they are < 16 (max value from the 4 buttons/switches)

            // Attempts to speed up the retrival process by making the data within the chunk "Accessed" by setting its start and end bits
            // to 0, which will effectively make this chunck invisible
            USARTInput[i] = 0;
            USARTInput[i+5] = 0;

            data_ordered[0] = USARTInput[i+1];
            data_ordered[1] = USARTInput[i+2];
            data_ordered[2] = USARTInput[i+3];
            data_ordered[3] = USARTInput[i+4];
            i = sizeof(USARTInput)+1;
            
        }

    }
    // Exit interupt
    HAL_DMA_IRQHandler(&hdma_usart3_rx);
}

// Receive incomming byte, CALL EVERY 25 ms
void usart_receive(void){

    // Receive data from DMA transfer, by default this is 10 bytes where the 4 bytes of desired data is extracted
    HAL_UART_Receive_DMA(&husart3, USARTInput, sizeof(USARTInput));
    
    // Process the data from the usart receive, will send motor speeds and various settings where needed
    usart_process();

}

// Process the data from the usart receive, will send motor speeds and various settings where needed
// At the moment, print data to screen and do 'toy' controlling
void usart_process(void){

    // Received check, for debugging only, remove print statements once fully tested
    
    for (int i = 0; i < 4; i++)
    {
        printf("%d  ",data_ordered[i]);
        
    }
    printf("\n");
    // Testing switch, ultrasonic and servo, and left slider
    if (data_ordered[3] && 1 == 1){
        servo_1_set(data_ordered[0]);
    } else {
        servo_1_set( US_get_distance()*3);
    }
   
}

// This function is called to send the data to the app, CALL EVERY 25 ms.
// Not using interupt or DMA may change to interupt later
void usart_transmit(void){

    get_data_to_transmit(); // get the data to transmit and store it into the 4 byte [1], [2], [3], [4] of the data_send array
    data_send[0] = 255; // ensure that data_send[0] is 255 (start bit)
    HAL_UART_Transmit(&husart3, data_send, 5, 50); // send the data, currently is blocking, need to make non-blocking

}

// At the moment the data to send to the app, is just the 4 bytes that were received
// Get data process, at the moment is the for loop below, this will change to calling various functions to gather the data required
void get_data_to_transmit(void){
    

    for (int i = 0; i < 4; i++)
    {
        data_send[i+1] = data_ordered[i];
    }
    data_send[3] = US_get_distance();
}






