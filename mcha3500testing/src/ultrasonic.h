#ifndef ULTRASONIC_H
#define ULTRASONIC_H

void US_trig(void);
void ultrasonic_init(void);
void US_echo(void);
void US_test(void);
uint8_t US_get_distance(void);


#endif