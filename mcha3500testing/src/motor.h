#ifndef MOTOR_H
#define MOTOR_H

void motor_PWM_init(void);
void motor_encoder_init(void);
int32_t motor_encoder_getValue(void);


#endif