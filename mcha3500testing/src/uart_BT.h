#ifndef UART_BT_H
#define UART_BT_H

void usart_BT_init(void);
void usart_receive(void);
void usart_transmit(void);
void received(void);
void get_data_to_transmit(void);
void usart_process(void);

void usart_receive_thread_init(void);
void usart_transmit_thread_init(void);


#endif