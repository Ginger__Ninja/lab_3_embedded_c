#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "data_logging.h"
#include "pendulum.h"
#include "IMU.h"



/* Local Function prototypes */

static void (*log_function)(void);


static void log_pointer(void *argument);



/* RTOS Init variables*/
// Declare timer id and attr variables
osTimerId_t     _logging_timer_id;  
osTimerAttr_t   _logging_timer_attr = 
{
    .name = "loggingTimer"
};


/* Variable declarations */

uint16_t logCount;
float pot;
float angle, gyro;



/* Functions */

static void log_pendulum(void *argument){
    UNUSED(argument);

    pot = pendulum_read_voltage();

    printf("%f,%.2f\n",logCount*0.005,pot);

    logCount++;


    if (logCount*0.005 == 2) logging_stop();

}


void logging_init(void){
    // Init a periodic timer with the log_pendulum function as the call back, onces period is up
    _logging_timer_id = osTimerNew(log_pointer, osTimerPeriodic, NULL, &_logging_timer_attr);
    
}

static void log_pointer(void *argument){
    UNUSED(argument);

    // Call function pointed to by log_function
    (*log_function)();


}


void logging_start(void){
    
    log_function = &log_pendulum;


    if(!osTimerIsRunning(_logging_timer_id)){
        // Init log count to zero for new logging period
        logCount = 0;

        // Start 200 Hz timer
        osTimerStart(_logging_timer_id, 5U); // period = 1/frequency, 5U is 0.005/sec or 200 Hz

    }

}


void logging_stop(void){

    if(osTimerIsRunning(_logging_timer_id)){
        // Stop timer
        osTimerStop(_logging_timer_id);

    }

}

static void log_imu(void *argument){
    UNUSED(argument);

    IMU_read();

    angle = get_acc_angle();

    gyro = get_gyroX();
    
    pot = pendulum_read_voltage();

    printf("%f,%f,%f,%f\n",logCount*0.005,angle,gyro,pot);

    logCount++;

    if (logCount*0.005 == 5) logging_stop();

}

void imu_logging_start(void){

    log_function = &log_imu;

    if(!osTimerIsRunning(_logging_timer_id)){
        // Init log count to zero for new logging period
        logCount = 0;

        // Start 200 Hz timer
        osTimerStart(_logging_timer_id, 5U); // period = 1/frequency, 5U is 0.005/sec or 200 Hz

    }

}