#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "tm_stm32_mpu6050.h"
#include "IMU.h"
#include <math.h>




TM_MPU6050_t IMU_datastruct;
int16_t raw_IMU_Y,raw_IMU_X,raw_IMU_Z;
float accY, gyroX, accZ;
double theta;


void IMU_init(void){
    
    // Initialise IMU with required settings
    TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void){

    TM_MPU6050_ReadAll(&IMU_datastruct);

}

float get_accY(void){

    raw_IMU_Y = IMU_datastruct.Accelerometer_Y;
    accY = (((float)raw_IMU_Y/32767.0)*4.0*9.81);

    return accY;
}

float get_accZ(void){

    raw_IMU_Z = IMU_datastruct.Accelerometer_Z;

    accZ = (((float)raw_IMU_Z/32767.0)*4.0*9.81);

    return accZ;
}


float get_gyroX(void){

    raw_IMU_X = IMU_datastruct.Gyroscope_X; 

    gyroX = (((float)raw_IMU_X/32767.0)*250.0*(M_PI/180.0));

    return gyroX;
}

double get_acc_angle(void){

    theta = -atan2(get_accZ(),get_accY());

    return theta;

}


