#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

#include "motor.h"

static TIM_HandleTypeDef htim3;
static TIM_OC_InitTypeDef sConfigPWM;

static int32_t encoder_count;

uint8_t pin0, pin1;


void motor_PWM_init(void){

    // Enable timer 3 clock
    __HAL_RCC_TIM3_CLK_ENABLE();

    // Enable GPIOA clock
    __HAL_RCC_GPIOA_CLK_ENABLE();

    // Initialise PA6 as required
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_6;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;

    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

    // Initialise timer 3 as required
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 10000;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;

    // Configure timer 3 channel 1 as required
    sConfigPWM.OCMode = TIM_OCMODE_PWM1;
    sConfigPWM.Pulse = 0;
    sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;

    //  Initialise timer 3 in PWM mode
    HAL_TIM_PWM_Init(&htim3);

    // Configure timer 3 channel 1 in PWM mode
    HAL_TIM_PWM_ConfigChannel(&htim3,&sConfigPWM,TIM_CHANNEL_1);

    // Set compare value such that at 25% duty cycle //////////////////////////////////////////////////////NEED TO CHECK IN LAB/////////
    __HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,2500);

    // Start PWM timer
    HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);



}

void motor_encoder_init(void){

    // Enable GPIOC clock
    __HAL_RCC_GPIOC_CLK_ENABLE();   

    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    // Set interupt priority lines 0,1 -> 0x0F, 0x0F
    HAL_NVIC_SetPriority(EXTI0_IRQn,0x0F,0x0F);
    HAL_NVIC_SetPriority(EXTI1_IRQn,0x0F,0x0F);

    // Enable external interupt
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);




}

/* Interupt for GPIOC pin 0 */
void EXTI0_IRQHandler(void){

    pin0 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
    pin1 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);

    if (pin0 == pin1){
        encoder_count++;
    } else {
        encoder_count--;
    }

    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

/* Interupt for GPIOC pin 1 */
void EXTI1_IRQHandler(void){

    pin0 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
    pin1 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);

    if (pin0 == pin1){
        encoder_count--;
    } else {
        encoder_count++;
    }

    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
    
}

/* Return encoder count as int32_t */
int32_t motor_encoder_getValue(void){

    return encoder_count;
}