#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

#include "pendulum.h"


static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;
static DMA_HandleTypeDef hdma_adc2;

uint16_t DMA_potVal;
uint16_t ADCInput[1];

void pendulum_init(void){

    // Enable ADC1 clock
    __HAL_RCC_ADC1_CLK_ENABLE();

    // Enable GPIOB clock
    __HAL_RCC_GPIOB_CLK_ENABLE();

    // Initialise PB0 as required
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_0;
    GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    // Initialise ADC1 as required
    hadc1.Instance = ADC1;
    hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc1.Init.Resolution = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ScanConvMode = ENABLE;
    hadc1.Init.ContinuousConvMode = ENABLE;
    hadc1.Init.DMAContinuousRequests = ENABLE;
    hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    hadc1.Init.NbrOfConversion = 1;




    // Configure the ADC channel as required
    sConfigADC.Channel = ADC_CHANNEL_8;
    sConfigADC.Rank = 1;
    sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    sConfigADC.Offset = 0;

    // Initialise the ADC                    
    HAL_ADC_Init(&hadc1);
    // Configure the ADC channel             
    HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);


    /* DMA controller clock enable */
    __HAL_RCC_DMA2_CLK_ENABLE();

    /* DMA Initialise */
    hdma_adc2.Instance = DMA2_Stream0;
    hdma_adc2.Init.Channel = DMA_CHANNEL_0;
    hdma_adc2.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc2.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc2.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc2.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD; // 16 bits
    hdma_adc2.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD; // 16 bits
    hdma_adc2.Init.Mode = DMA_CIRCULAR; // Repeats data collection continuously
    hdma_adc2.Init.Priority = DMA_PRIORITY_LOW;
        
    HAL_DMA_Init(&hdma_adc2);

    /* Link the DMA to the ADC */
    __HAL_LINKDMA(&hadc1, DMA_Handle, hdma_adc2);

    /* Enable interupt handler to handle DMA transfer completion*/
    // When this occurs set the retrieved values into DMA_temp, DMA_speed, DMA_voltage variables
    HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

    /* Start DMA and ADC at the same time */
    // Define ADCInputs as destination for DMA, this has 3 uint16_t values in it
    HAL_ADC_Start_DMA(&hadc1, (uint32_t*)ADCInput, 1);




}

float pendulum_read_voltage(void){

    // Return from ADC and corresponding voltage
    float voltage;

    // Convert 0-4095 to voltage;
    voltage = (float)DMA_potVal * 3.3/4095.0;

    return voltage;

}


void DMA2_Stream0_IRQHandler(void){

    DMA_potVal = ADCInput[0];

    HAL_DMA_IRQHandler(&hdma_adc2);
}
