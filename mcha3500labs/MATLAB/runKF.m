clear
clc
close all

%% Load data

load('imuData_moving1');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200))
acc_angle_init = mean(accAngle(1:200))
potAngle = potAngle + (acc_angle_init-pot_angle_init);


N = length(potAngle);

[x_KF,P_KF] = runKF_func(accAngle, gyroVelocity, N);

figure(1)

plot(time,potAngle,time,x_KF(:,2),time,accAngle);
leg = legend('Pot angle','KF angle','Accel Angle','location','best');

grid on;

%% Functions

function [x_KF, P_KF] = runKF_func(angle_accel1, velocity_gyro, N)

    % State vector: [dtheta; theta], linearised about pi/2 for vertical
    Ac = [0 0 0; 1 0 0; 0 0 0];
    Bc = [0;0;0]; % Input set to none
    C = [0 1 0; 1 0 1];
    
    % Discretise model
    T = 1/200;
    [Ad, ~] = c2d(Ac,Bc,T);
    
    % Define Kalman filter initial conditions
    % Initial state estimate
    xm = [0; 0; 0];
    
    % Initial estimate error covariance
    Pm = 1*eye(3);
    
    % Measurement noise covariance
    R = chol([0.0502 0; 0 3.4546e-06]);
    
    % Process noise covariance
    Q = [1e-6 0 0; 0 1e-6 0; 0 0 1e-6];
    
    % Allocate space to save results
    x_KF = zeros(N,3);
    P_KF = zeros(3,3,N);
    
    % Run Kalman Filter
    for i=1:N
        % Pack measurement vector
        yi = [angle_accel1(i);velocity_gyro(i)];
        
        % Correction step
        % Compute Kalman gain
        Kk = Pm*C'/(C*Pm*C' + R);
        
        % compute corrected state estimate
        xp = xm + Kk*(yi - C*xm);
        
        % Compute new measurement error covariance
        Pp = (eye(3) -Kk*C)*Pm*(eye(3) - Kk*C)' +Kk*R*Kk';
        
        % Prediction step
        % Predict next state
        xm = Ad*xp;
        
        % Compute prediction error covariance
        Pm = Ad*Pp*Ad' + Q;
        
        % Store results
        x_KF(i,:) = xp.';
        P_KF(:,:,i) = Pm;
        
    end
    
end










