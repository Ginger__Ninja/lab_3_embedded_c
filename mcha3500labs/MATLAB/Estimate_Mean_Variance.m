clear
clc
close all

%% Load data
load('imuData_still');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;

gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
%% Compute sample mean and variance

disp("Gyro")

% Compute sample mean
sample_mean = mean(gyroVelocity)

% Compute sample variance. Use Bessel's correction
tally = 0;
for i = 1:length(gyroVelocity)
    
    tally = (gyroVelocity(i)-sample_mean)*(gyroVelocity(i)-sample_mean)' + tally;
    
end
sample_variance = (1/(length(gyroVelocity)-1))*tally


%% Load data

load('imuData_moving1');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200))
acc_angle_init = mean(accAngle(1:200))
potAngle = potAngle + (acc_angle_init-pot_angle_init);

figure(1);
plot(time,accAngle,time,potAngle)
% Compute the angle measurement 'noise'
angle_noise = accAngle - potAngle;

%% Compute sample mean and covariance
% Compute sample mean
disp("Angle")

sample_mean = mean(angle_noise)

% Compute sample variance. Use Bessel's correction
tally = 0;
for i = 1:length(angle_noise)
    
    tally = (angle_noise(i)-sample_mean)*(angle_noise(i)-sample_mean)' + tally;
       
    
end
sample_variance = (1/(length(angle_noise)-1))*tally










