clear
clc
close all


%% Load data

load('imuData_moving1');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200))
acc_angle_init = mean(accAngle(1:200))
potAngle = potAngle + (acc_angle_init-pot_angle_init);

%% Set up optimisation
% Cose

get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);

cost = @(params) norm(potAngle - get_angle(runKF_func(accAngle, gyroVelocity, N, params))) ...
    + norm(gyroVelocity - get_gyro(runKF_func(accAngle, gyroVelocity, N, params)));

params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
options = optimoptions('fminunc','MaxFunctionEvaluations',2000);
params_opt = fminunc(cost,params0,options);

L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)]

Q_opt = L_opt*L_opt'

N = length(potAngle);

[x_KF,P_KF] = runKF_func(accAngle, gyroVelocity, N, params_opt);

figure(1)
subplot(2,1,1)
plot(time,potAngle,time,x_KF(:,2),time,accAngle);
title('1st data set')
legend('Pot angle','KF angle','Accel Angle','location','best');

grid on;

subplot(2,1,2)
plot(time,gyroVelocity,time,x_KF(:,1));
legend('Gyro velocity','KF velocity','location','best');

grid on;

%% Use 2nd data set

load('imuData_moving2');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200))
acc_angle_init = mean(accAngle(1:200))
potAngle = potAngle + (acc_angle_init-pot_angle_init);

[x_KF,P_KF] = runKF_func(accAngle, gyroVelocity, N, params_opt);

figure(2)
subplot(2,1,1)
plot(time,potAngle,time,x_KF(:,2),time,accAngle);
title('2nd data set')
legend('Pot angle','KF angle','Accel Angle','location','best');

grid on;

subplot(2,1,2)
plot(time,gyroVelocity,time,x_KF(:,1) + x_KF(:,3));
legend('Gyro velocity','KF velocity','location','best');

grid on;


%% Use still data

load('imuData_still');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200))
acc_angle_init = mean(accAngle(1:200))
potAngle = potAngle + (acc_angle_init-pot_angle_init);

[x_KF,P_KF] = runKF_func(accAngle, gyroVelocity, N, params_opt);

figure(3)
subplot(2,1,1)
plot(time,potAngle,time,x_KF(:,2),time,accAngle);
title('Still data')
legend('Pot angle','KF angle','Accel Angle','location','best');

grid on;

subplot(2,1,2)
plot(time,gyroVelocity,time,x_KF(:,1) + x_KF(:,3));
legend('Gyro velocity','KF velocity','location','best');

grid on;

%% Functions

function [x_KF, P_KF] = runKF_func(angle_accel1, velocity_gyro, N, params)

    L = [params(1) 0 0; params(2) params(3) 0; params(4) params(5) params(6)];
    
    Q = (L*L');

    % State vector: [dtheta; theta], linearised about pi/2 for vertical
    Ac = [0 0 0; 1 0 0; 0 0 0];
    Bc = [0;0;0]; % Input set to none
    C = [0 1 0; 1 0 1];
    
    % Discretise model
    T = 1/200;
    [Ad, ~] = c2d(Ac,Bc,T);
    
    % Define Kalman filter initial conditions
    % Initial state estimate
    xm = [0; 0; 0];
    
    % Initial estimate error covariance
    Pm = 1*eye(3);
    
    % Measurement noise covariance
    R = chol([0.0502 0; 0 3.4546e-06]);
    
    % Allocate space to save results
    x_KF = zeros(N,3);
    P_KF = zeros(3,3,N);
    
    % Run Kalman Filter
    for i=1:N
        % Pack measurement vector
        yi = [angle_accel1(i);velocity_gyro(i)];
        
        % Correction step
        % Compute Kalman gain
        Kk = Pm*C'/(C*Pm*C' + R);
        
        % compute corrected state estimate
        xp = xm + Kk*(yi - C*xm);
        
        % Compute new measurement error covariance
        Pp = (eye(3) -Kk*C)*Pm*(eye(3) - Kk*C)' +Kk*R*Kk';
        
        % Prediction step
        % Predict next state
        xm = Ad*xp;
        
        % Compute prediction error covariance
        Pm = Ad*Pp*Ad' + Q;
        
        % Store results
        x_KF(i,:) = xp.';
        P_KF(:,:,i) = Pm;
        
    end
    
end










