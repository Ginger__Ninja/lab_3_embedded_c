clear
clc
%% Load data
load('imuData_moving1');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% TODO: Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle + (acc_angle_init - pot_angle_init);


%% Set up optimisation
% Define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) + norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
options = optimoptions('fminunc','MaxFunctionEvaluations',2000);
params_opt = fminunc(cost,params0,options);
L_opt = [params(1) 0 0; params(2) params(3) 0; params(4) params(5) params(6)];
Q_opt = L_opt * L_opt.';

[x_KF, P_KF] = func(accAngle, gyroVelocity, N, params);

subplot(time,accAngle,time,potAngle,time,x_KF(:,2))
legend('Accelerometer Angle', 'Potentiometer Angle', 'Kalman Filter Angle','Location','southwest')

%% Functions
function [x_KF, P_KF] = func(angle_accel, velocity_gyro, N, params)

    % TODO: Unpack params into Cholesky decomposition of Q
    L = [params(1) 0 0; params(2) params(3) 0; params(4) params(5) params(6)];
    % TODO: Compute process noise Q from L
    Q = L * L.';
    
    % TODO: Define contimuout time model
    Ac = [0 0 0 ; 1 0 0; 0 0 0];
    Bc = [0 0 0; 0 0 0; 0 0 0];
    C = [0 1 0; 1 0 1];
    % TODO: Discretise model
    T = 1/200;
    [Ad, Bd] = c2d(Ac,Bc,T);
    % Define Kalman filter initial conditions
    % Initial state estimate
    xm = [0; 0; 0];
    % Initial estimate error covariance
    Pm = 1*eye(3);
    % TODO: Measurement noise covariance
    R = chol([0.0502 0; 0 3.4546e-06]);
    % TODO: Process noise covariance
    Q = [1e-6 0 0; 0 1e-6 0; 0 0 1e-6];
    % Allocate space to save results
    x_KF = zeros(N,3);
    P_KF = zeros(3,3,N);
    % Run Kalman Filter

    for i=1:N
        % TODO: Pack measurement vector
        yi = [angle_accel(i); velocity_gyro(i)];
        % Correction step
        % TODO: Compute Kalman gain
        Kk = Pm*C.'/(C*Pm*C.' + R);
        % TODO: compute corrected state estimate
        xp = xm + Kk*(yi - C*xm);
        % TODO: Compute new measurement error covariance
        Pp = (eye(3) - Kk*C)*Pm*(eye(3) - Kk*C).' + Kk*R*Kk.';
        % Prediction step
        % TODO: Predict next state
        xm = Ad*xp;
        % TODO: Compute prediction error covariance
        Pm = Ad*Pp*Ad.' + Q;
        % Store results
        x_KF(i,:) = xp.';
        P_KF(:,:,i) = Pm;
    end
end
